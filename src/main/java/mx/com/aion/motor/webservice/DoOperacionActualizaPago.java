
package mx.com.aion.motor.webservice;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for doOperacionActualizaPago complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="doOperacionActualizaPago"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="folioTransaccion" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="estatusPago" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *         &lt;element name="devolucionId" type="{http://www.w3.org/2001/XMLSchema}int" minOccurs="0"/&gt;
 *         &lt;element name="devolucionDesc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "doOperacionActualizaPago", propOrder = {
    "folioTransaccion",
    "estatusPago",
    "devolucionId",
    "devolucionDesc"
})
public class DoOperacionActualizaPago {

    protected String folioTransaccion;
    protected String estatusPago;
    protected Integer devolucionId;
    protected String devolucionDesc;

    /**
     * Gets the value of the folioTransaccion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFolioTransaccion() {
        return folioTransaccion;
    }

    /**
     * Sets the value of the folioTransaccion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFolioTransaccion(String value) {
        this.folioTransaccion = value;
    }

    /**
     * Gets the value of the estatusPago property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstatusPago() {
        return estatusPago;
    }

    /**
     * Sets the value of the estatusPago property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstatusPago(String value) {
        this.estatusPago = value;
    }

    /**
     * Gets the value of the devolucionId property.
     * 
     * @return
     *     possible object is
     *     {@link Integer }
     *     
     */
    public Integer getDevolucionId() {
        return devolucionId;
    }

    /**
     * Sets the value of the devolucionId property.
     * 
     * @param value
     *     allowed object is
     *     {@link Integer }
     *     
     */
    public void setDevolucionId(Integer value) {
        this.devolucionId = value;
    }

    /**
     * Gets the value of the devolucionDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDevolucionDesc() {
        return devolucionDesc;
    }

    /**
     * Sets the value of the devolucionDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDevolucionDesc(String value) {
        this.devolucionDesc = value;
    }

}
