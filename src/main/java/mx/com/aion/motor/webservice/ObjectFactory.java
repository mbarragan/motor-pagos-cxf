
package mx.com.aion.motor.webservice;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mx.com.aion.motor.webservice package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _DoOperacionPago_QNAME = new QName("http://webservice.motor.aion.com.mx/", "doOperacionPago");
    private final static QName _DoOperacionPagoResponse_QNAME = new QName("http://webservice.motor.aion.com.mx/", "doOperacionPagoResponse");
    private final static QName _DoOperacionActualizaPago_QNAME = new QName("http://webservice.motor.aion.com.mx/", "doOperacionActualizaPago");
    private final static QName _DoOperacionActualizaPagoResponse_QNAME = new QName("http://webservice.motor.aion.com.mx/", "doOperacionActualizaPagoResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: mx.com.aion.motor.webservice
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link DoOperacionPago }
     * 
     */
    public DoOperacionPago createDoOperacionPago() {
        return new DoOperacionPago();
    }

    /**
     * Create an instance of {@link DoOperacionPagoResponse }
     * 
     */
    public DoOperacionPagoResponse createDoOperacionPagoResponse() {
        return new DoOperacionPagoResponse();
    }

    /**
     * Create an instance of {@link DoOperacionActualizaPago }
     * 
     */
    public DoOperacionActualizaPago createDoOperacionActualizaPago() {
        return new DoOperacionActualizaPago();
    }

    /**
     * Create an instance of {@link DoOperacionActualizaPagoResponse }
     * 
     */
    public DoOperacionActualizaPagoResponse createDoOperacionActualizaPagoResponse() {
        return new DoOperacionActualizaPagoResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoOperacionPago }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.motor.aion.com.mx/", name = "doOperacionPago")
    public JAXBElement<DoOperacionPago> createDoOperacionPago(DoOperacionPago value) {
        return new JAXBElement<DoOperacionPago>(_DoOperacionPago_QNAME, DoOperacionPago.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoOperacionPagoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.motor.aion.com.mx/", name = "doOperacionPagoResponse")
    public JAXBElement<DoOperacionPagoResponse> createDoOperacionPagoResponse(DoOperacionPagoResponse value) {
        return new JAXBElement<DoOperacionPagoResponse>(_DoOperacionPagoResponse_QNAME, DoOperacionPagoResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoOperacionActualizaPago }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.motor.aion.com.mx/", name = "doOperacionActualizaPago")
    public JAXBElement<DoOperacionActualizaPago> createDoOperacionActualizaPago(DoOperacionActualizaPago value) {
        return new JAXBElement<DoOperacionActualizaPago>(_DoOperacionActualizaPago_QNAME, DoOperacionActualizaPago.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link DoOperacionActualizaPagoResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://webservice.motor.aion.com.mx/", name = "doOperacionActualizaPagoResponse")
    public JAXBElement<DoOperacionActualizaPagoResponse> createDoOperacionActualizaPagoResponse(DoOperacionActualizaPagoResponse value) {
        return new JAXBElement<DoOperacionActualizaPagoResponse>(_DoOperacionActualizaPagoResponse_QNAME, DoOperacionActualizaPagoResponse.class, null, value);
    }

}
